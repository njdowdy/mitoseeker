import os
import subprocess
from concurrent.futures import ThreadPoolExecutor
import itertools
import glob
import logging
from datetime import datetime
from concurrent.futures import ProcessPoolExecutor  # Import ProcessPoolExecutor


def setup_logger() -> logging.Logger:
    log_filename = f"/app/logs/{datetime.now().strftime('%Y%m%d_%H%M%S')}.log"
    logging.basicConfig(
        filename=log_filename,
        level=logging.INFO,
        format="%(asctime)s [PID: %(process)d] [%(levelname)s]: %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
    )
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    formatter = logging.Formatter(
        "%(asctime)s [PID: %(process)d] [%(levelname)s]: %(message)s"
    )
    console.setFormatter(formatter)
    logging.getLogger().addHandler(console)
    return logging.getLogger()


def process_folder(
    taxon_id: str, date_folder: str, folder_path: str, logger: logging.Logger
) -> None:
    iterations = 1

    fastq_files = glob.glob(f"{folder_path}/*_R[1-2]_*.fastq.gz")
    if not fastq_files:
        return

    r1_file = next(f for f in fastq_files if "_R1_" in f)
    r2_file = next(f for f in fastq_files if "_R2_" in f)
    basename = os.path.basename(r1_file).replace("_R1_", "_interleaved_")
    interleaved_file = os.path.join(folder_path, basename)

    if not os.path.exists(interleaved_file):
        logger.info(f"🍃 Interleaving R1 and R2 read archives for {taxon_id}...")
        result = subprocess.run(
            [
                "/usr/local/bin/BBMap/reformat.sh",
                f"in1={r1_file}",
                f"in2={r2_file}",
                f"out={interleaved_file}",
            ],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True,
        )
        logger.info(result.stdout)
        if result.stderr:
            logger.error(result.stderr)
        logger.info(f"🏁 Interleaving complete for {taxon_id}!")
    else:
        logger.info(f"⏭️ Interleaved file already exists for {taxon_id}. Skipping...")

    # STEP 2: Check if MITObim results already exist
    output_folder = f"/app/output/{taxon_id}/RUN_0001"
    if not os.path.exists(output_folder):
        logger.info(f"🛫 Starting MITObim on {taxon_id}...")
        os.makedirs(output_folder)
        result = subprocess.run(
            [
                "/app/MITObim/MITObim.pl",
                "--mirapath",
                "/usr/local/bin/mira4/bin/",
                "-quick",
                "/app/resources/references/reference_coi.fasta",
                "-readpool",
                interleaved_file,
                "--pair",
                "-start",
                "0",
                "-end",
                f"{iterations}",
                "-sample",
                taxon_id,
                "-ref",
                taxon_id,
            ],
            cwd=output_folder,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True,
        )
        logger.info(result.stdout)
        if result.stderr:
            logger.error(result.stderr)
        logger.info(f"🏁 MITObim on {taxon_id} completed!")
    else:
        logger.info(f"⏭️ MITObim results already exist for {taxon_id}. Skipping...")

    # STEP 3: Concatenate the resulting fasta file to the overall results file
    results_file = "/app/output/compiled_results.fasta"
    fasta_file_glob = os.path.join(
        output_folder, f"iteration{iterations}", "*_noIUPAC.fasta"
    )
    fasta_files = glob.glob(fasta_file_glob)
    if fasta_files:
        fasta_file = fasta_files[0]
        if not os.path.exists(results_file):
            with open(results_file, "w") as f:
                f.write("")
        with open(results_file, "a") as results_f:
            results_f.write(f">{taxon_id}\n")
            with open(fasta_file, "r") as fasta_f:
                second_line = list(itertools.islice(fasta_f, 1, 2))[0].strip()
                results_f.write(f"{second_line}\n")

    # STEP 4: Delete the interleaved reads
    if os.path.exists(interleaved_file):
        os.remove(interleaved_file)


def run_job(taxon_id: str, logger: logging.Logger) -> None:
    taxon_folder = f"/app/resources/merged_reads/{taxon_id}"

    folder_contents = os.listdir(taxon_folder)
    for folder_name in folder_contents:
        if folder_name != ".DS_Store":
            folder_path = os.path.join(taxon_folder, folder_name)

            if os.path.isdir(folder_path):
                process_folder(taxon_id, folder_name, folder_path, logger)
            else:
                # Assuming it's a file or the "date" folder is optional
                process_folder(taxon_id, "", taxon_folder, logger)


if __name__ == "__main__":
    logger = setup_logger()
    taxon_ids = [
        x for x in os.listdir("/app/resources/merged_reads/") if ".DS_Store" not in x
    ]
    with ProcessPoolExecutor(max_workers=2) as executor:  # Use ProcessPoolExecutor
        executor.map(run_job, taxon_ids, itertools.repeat(logger))
